#include "daemon.h"

static volatile sig_atomic_t terminated = 0;
static struct sockaddr_un addr;

void sighandler(int signum)
{
  fprintf (stderr, "<%d> Signal %d received\n", LOG_INFO, signum);

  if (signum == SIGTERM){
    fprintf (stderr, "<%d> Termination received, exiting...\n", LOG_INFO);
    terminated = 1;
  }
}

void daemonize(void)
{
  pid_t pid;
  pid = fork();

  if (pid < 0 || pid > 0 ) {
    fprintf(stderr, " <%d> %s\n", pid < 0 ? LOG_ERR : LOG_INFO,
            pid < 0 ? "Daemon failed to initialize" : "Fork successful");
    exit(pid < 0 ? EXIT_FAILURE : EXIT_SUCCESS);
  }

  if (setsid() < 0) {
    fprintf(stderr, " <%d> Daemon failed to initialize\n", LOG_ERR);
    exit(EXIT_FAILURE);
  }

  pid = fork();
  if (pid < 0 || pid > 0 ) {
    fprintf(stderr, " <%d> %s\n", pid < 0 ? LOG_ERR : LOG_INFO,
            pid < 0 ? "Daemon failed to initialize" : "Fork successful");
    exit(pid < 0 ? EXIT_FAILURE : EXIT_SUCCESS);
  }

  umask(0);
  chdir("/");
  for (long i = sysconf(_SC_OPEN_MAX); i >= 0; i-- ) {
    close((int) i);
  }
}

int init_domain_socket( char *socket_path)
{
  int fd = socket(AF_UNIX, SOCK_STREAM, 0);

  memset(&addr, 0, sizeof(struct sockaddr_un));
  addr.sun_family = AF_UNIX;
  memset(addr.sun_path, 0, UNIX_PATH_MAX);
  snprintf(addr.sun_path, UNIX_PATH_MAX, "%s", socket_path);

  bind (fd, (struct sockaddr *)&addr, sizeof(struct sockaddr_un));

  return fd;
}


void term_domain_socket(void)
{
  /* stub */
}

int main (void)
{
  daemonize();
  fprintf(stderr, "<%d> Daemon initialized", LOG_INFO);

  /* Handle SIGTERM */
  struct sigaction action;
  memset(&action, 0, sizeof(struct sigaction));
  action.sa_handler = sighandler;

  if (sigaction(SIGTERM, &action, NULL) == -1) {
    fprintf(stderr, "<%d> Unable to handle signal, exiting...\n", LOG_ERR );
    return EXIT_FAILURE;
  }

  int fd0 = init_domain_socket("socket");
  int fd1 = 0;
  listen(fd0, 5);
  while (!terminated) {
    fd1 = accept(fd0, NULL, NULL);

    close(fd1);
  }
  close(fd0);
  unlink("socket");

  fprintf(stderr, "<%d> Daemon exiting\n", LOG_INFO);
  return EXIT_SUCCESS;
}
