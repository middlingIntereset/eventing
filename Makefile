SHELL := /bin/bash
CC ?= gcc
LD ?= gcc

SRCDIR = src
DAEMON_SRCDIR= ${SRCDIR}/daemon
CLIENT_SRCDIR= ${SRCDIR}/client

OBJDIR = obj
DAEMON_OBJDIR=${OBJDIR}/daemon
CLIENT_OBJDIR=${OBJDIR}/client

BUILD := debug
BUILD_DIR := ${CURDIR}/${BUILD}

TEST := test
TEST_DIR := ${CURDIR}/${TEST}
DAEMON_TEST_DIR := $(TEST_DIR)/daemon
CLIENT_TEST_DIR := $(TEST_DIR)/client

BUILD_TEST_DIR := ${BUILD_DIR}/${TEST}

CFLAGS := ${CFLAGS} -std=gnu99 -pedantic -Werror -Wall -Wextra -Wcast-align\
	-Wcast-qual -Wdisabled-optimization -Wformat=2 -Winit-self\
	-Wmissing-include-dirs -Wredundant-decls -Wshadow\
	-Wstrict-overflow=5 -Wundef -fdiagnostics-show-option -Wconversion -g \
	-O0 -fstack-protector-all -pthread
CLFAGS_RELEASE := ${CFLAGS} -O3 -DNDEBUG
TEST_FLAGS := -lcriterion
LDFLAGS := -lm

BINARY := $(BUILD_DIR)/eventing
DAEMON_BINARY := $(BUILD_DIR)/daemon
CLIENT_BINARY := $(BUILD_DIR)/client

PREFIX  ?= /usr/local
BINDIR  ?= ${PREFIX}/bin
DATADIR ?= ${PREFIX}/share
MANDIR  ?= ${DATADIR}/man

# Add source epoch date for reporducible builds
DATE_FMT = %Y-%m-%d
ifdef SOURCE_DATE_EPOCH
        BUILD_DATE ?= $( date -u -d "@$(SOURCE_DATE_EPOCH)" "+$(DATE_FMT)"  2>/dev/null || date -u -r "$(SOURCE_DATE_EPOCH)" "+$(DATE_FMT)" 2>/dev/null || date -u "+$(DATE_FMT)")
else
        BUILD_DATE ?= $(shell date "+$(DATE_FMT)")
endif

# Source files
DAEMON_SRCS = $(sort $(wildcard $(DAEMON_SRCDIR)/*.c))
CLIENT_SRCS = $(sort $(wildcard $(CLIENT_SRCDIR)/*.c))

# Objects Files
DAEMON_OBJS = $(patsubst $(DAEMON_SRCDIR)/%.c,$(DAEMON_OBJDIR)/%.o,$(DAEMON_SRCS))
CLIENT_OBJS = $(patsubst $(CLIENT_SRCDIR)/%.c,$(CLIENT_OBJDIR)/%.o,$(CLIENT_SRCS))

# Test Files
DAEMON_TSTS = $(filter-out $(DAEMON_TEST_DIR)/unit_main.c, $(patsubst $(DAEMON_SRCDIR)/%.c,$(DAEMON_TEST_DIR)/unit_%.c,$(DAEMON_SRCS)))
CLIENT_TSTS = $(filter-out $(CLIENT_TEST_DIR)/unit_main.c, $(patsubst $(CLIENT_SRCDIR)/%.c,$(CLIENT_TEST_DIR)/unit_%.c,$(CLIENT_SRCS)))

# Test Binaries
DAEMON_TST_BINS = $(patsubst $(DAEMON_TEST_DIR)/%.c,$(BUILD_TEST_DIR)/%,$(DAEMON_TSTS))
CLIENT_TST_BINS = $(patsubst $(CLIENT_TEST_DIR)/%.c,$(BUILD_TEST_DIR)/%,$(CLIENT_TSTS))


# Targets
default: all
all: daemon client tests
daemon: $(DAEMON_BINARY)
client: $(CLIENT_BINARY)
tests: $(DAEMON_TST_BINS) $(CLIENT_TST_BINS)
run_tests:
	$(DAEMON_TST_BINS)
	$(CLIENT_TST_BINS)
print-%  : ; @echo $* = $($*)

$(DAEMON_BINARY): $(DAEMON_OBJS)
	@echo "Building Daemon Binary"
	@mkdir -p $(BUILD_DIR)
	@$(CC) $(LDFLAGS) -o $@ $+

$(DAEMON_OBJS): $(DAEMON_OBJDIR)/%.o: $(DAEMON_SRCDIR)/%.c
	@echo Compiling $<
	@mkdir -p $(DAEMON_OBJDIR)
	@$(CC) ${CFLAGS} -c $< -o $@

$(CLIENT_BINARY): $(CLIENT_OBJS)
	@echo "Building Client Binary"
	@mkdir -p $(BUILD_DIR)
	@$(CC) $(LDFLAGS) -o $@ $+

$(CLIENT_OBJS): $(CLIENT_OBJDIR)/%.o: $(CLIENT_SRCDIR)/%.c
	@echo Compiling $<
	@mkdir -p $(CLIENT_OBJDIR)
	@$(CC) ${CFLAGS} -c $< -o $@

$(DAEMON_TST_BINS): $(DAEMON_TSTS) $(DAEMON_SRCS)
	@echo Running all tests
	@mkdir -p $(BUILD_TEST_DIR)
	@echo $(CC) $(TEST_FLAGS) $+ -o $@
	@$(CC) $(TEST_FLAGS) $+ -o $@

$(CLIENT_TST_BINS): $(CLIENT_TSTS) $(CLIENT_SRCS)
	@echo Running all tests
	@mkdir -p $(BUILD_TEST_DIR)
	@echo $(CC) $(TEST_FLAGS) $+ -o $@
	@$(CC) $(TEST_FLAGS) $+ -o $@

run: $(BINARY)
	$(BINARY)

.PHONEY: clean
clean:
	rm -rf $(BUILD_DIR)
	rm -rf $(OBJDIR)
